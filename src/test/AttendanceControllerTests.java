package fi.vamk.e1900481.semdemo;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.AutoWired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunnen.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.e1900481.semdemo"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)


public class AttendanceControllerTests {

    @AutoWired
    private AttendanceRepository repository;

    @Test
    public void postGetDeleteAttendance() {
        Iterable<Attendance> begin = repository.findAll();
        System.out.println(IterableUtils.size(begin))
        //given
        Attendance att = new Attendance("ABCD");
        System.out.println("Att: " + att.toString());
        Attendance saved = repository.save(att);
        //test save
        System.out.println("Saved: " + saved.toString());
        //when
        Attendance found = repository.findByKey(att.getKey());
        System.out.println("Found: " + found.toString());
        //then
        assertThat(found.getKey().isEqualTo(att.getKey());
        repository.delete(found);
        System.out.println(IterableUtils.size(end))
        Iterable<Attendance> end = repository.findAll();
        assertEquals((long)IterableUtils.size(begin),(long)IterableUtils.size(end));

    }

    @Test
    public Attendance getByKeyAndDate(String date, String key) {
        if (repository.findByKey(key) == true) {
            Attendace a = repository.findByDate(att.getDate)
        } else {
            a = null;
        }
        return a;
    }
}
