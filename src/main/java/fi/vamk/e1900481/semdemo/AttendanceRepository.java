package fi.vamk.e1900481.semdemo;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);

    public Attendace findByDate(String date);
}