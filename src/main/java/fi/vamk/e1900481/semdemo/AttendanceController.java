package fi.vamk.e1900481.semdemo;

import org.springframework.beans.factory.annotation.AutoWired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AttendanceController {
    @AutoWired
    private AttendanceRepository repository;

    @GetMapping("/attendances")
    public Iterable<Attendance> allAttendances() {
        return repository.findAll();
    }

    @PostMappg("/attendance")
    public @ResponseBody Attendance create(@RequestBody Attendance item) 
        if (!item.getDate() != nulll) {
        return repository.save(item);
        }
    }

    @PutMapping("/attendance")
    public @ResponseBody Attendance update(@RequestBody Attendance item) {
        return repository.save(item);
    }

    @DeleteMapping("/attendance")
    public void delete(@RequestBody Attendance item) {
        repository.delete(item);
    }
}