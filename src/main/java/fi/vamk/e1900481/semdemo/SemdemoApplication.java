package fi.vamk.e1900481.semdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}

}
