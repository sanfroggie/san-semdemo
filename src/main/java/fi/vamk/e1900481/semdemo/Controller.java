package fi.vamk.e1900481.semdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class Controller {

    @GetMapping("/")
    public String swagger() {
        return "<script>window.location.replace(\"/swagger-ui.html\")</script>";
    }

    @GetMapping("/test")
    public String test() {
        return "{\"id\":1}";
    }
}